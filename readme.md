Cerebro
=============
Cerebro lze sprovoznit pomocí složky build a vytvořit uživatelé příkazem v basepresenteru
    
    $this->userRepository->createUser("user@solaris.media", "complexPassword");
    
Na co nezapomenout
=============
Vzdálená instalace, aktualizace a odinstalace je prováděná 
v jednom obrovském kódu který bude třeba ještě rozdělit na dílčí procesy, kód se 
nacházi ve dvou komponentách SiriusInstallationForm a SiriusUpdateForm, aktualizace
má vlastní komponentu jelikož je největší.

Tyto procesy budou škodu vloženy do nějakého manageru který bude pracovat z instanci siriuse a repositáři.
Instance siriuse obsahuje metody pro kontrolu připojení jak do ftp tak do databáze 
a každý proces managera by měl proběhnout až po kontrole připojení.

Na co teda nezapomenout
- Každý proces se je prováděn skrze malé soubory jenž instalaci, mažou a zálohují soubory,
tyto soubory se spuští vzdálené a tak před každám procesem je dočasně vymazán htaccess a index
- Instalace, aktualizace, odinstalace vždy vytvoří backup ftp do složky _backup
- Instalace se pokusí vytvořít kompletní schema databáze a nahraje všechny soubory znova
- Odinstalace maže všechny soubory
- Každý dlouhodobý job, by měl být spouštěn jako externí proces, jiným klientem, aby aktivní klient
mohl sledovat aktuální dění skrze ajax (nebude tak vyžadován multithread)
- Excludnout _backup slozku pri backupu

Aktualizace
=============
aktualizace se provádí bez výhrad vždy na testovací verzi, tato testovací verze je umístěna v kořenovém adresáři ftp.
Parálelně tak vždy existují dve verze, při aktualizaci se otestuje test verze a jakmile bdue vše ok, jen se vymění názvy slože
pro databázi je to složitější (domyslíme) ale všeobecná ide je taková

- Zálohuji aktuální verzi
- Vložím aktuální verzi ze zalohy na root-dev (nazev rootu + postfix dev)
- Aktualizuji dev verzi FTP
- Prekopiruji aktualni verzi sql na dbName-deb (nazev databaze + postfix dev)
- Aktualizuji dev verzi SQL
- Dev verze by mela byt dostupna na subdomene test.xxx.yy 



