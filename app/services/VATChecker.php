<?php
/**
 * User: Manwe
 */

namespace BiteIT;



class VATChecker
{
    const POST_ADDRESS = 'http://ec.europa.eu/taxation_customs/vies/vatResponse.html';
    const SOAP_ADDRESS = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';

    const METHOD_SOAP = 1;
    const METHOD_POST = 5;

    protected $type = self::METHOD_SOAP;

    public function __construct($type = self::METHOD_POST){
        $this->type = $type;
    }

    public function check($cc, $number){
        return $this->type == static::METHOD_SOAP ? $this->_checkSOAP($cc, $number) : $this->_checkPOST($cc, $number);
    }

    protected function _checkSOAP($cc, $number){
        $client = new \SoapClient(static::SOAP_ADDRESS);

        $params = ['countryCode' => $cc, 'vatNumber' => $number];
        $response = $client->checkVat($params);
        if($response)
        {
            return $response->valid;
        }
        return null;
    }

    protected function _checkPOST($cc, $number){
        $postdata = http_build_query(
            array(
                'memberStateCode' => $cc,
                'number' => $number
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context  = stream_context_create($opts);

        $result = file_get_contents(static::POST_ADDRESS, false, $context);

        if( stripos($result, 'Yes, valid VAT number') !== false ){
            return true;
        }
        else if( stripos($result, 'invalid') !== false ){
            return false;
        }
        return null;
    }

}