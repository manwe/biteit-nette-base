<?php
namespace App\Services;

class YouTubeService {

    private $apiKey;

    public function __construct($api_key)
    {
        $this->apiKey = $api_key;
    }

    public function parseVideoID($url){
        if (stripos($url,"youtube") !== false) {
            return explode("?v=", $url)[1];
        }
        if (stripos($url,  "youtu.be") !== false){
            return explode("youtu.be/", $url)[1];
        }
        return $url;
    }

    /**
     * @param $videoId
     * @return string
     */
    public function fetchAndParseData($videoId)
    {
        bd('loading video '.$videoId.' info from YouTube');
        $c = curl_init("https://www.googleapis.com/youtube/v3/videos?key=".
            $this->apiKey.
            "&part=statistics,contentDetails,statistics&id=$videoId");
        curl_setopt($c, CURLOPT_VERBOSE, true);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Referer: php7.ikonion.cz'));
        $data = curl_exec($c);
        if($data){
            $jsonData = json_decode($data, true)['items'];
            if (count($jsonData) <= 0)
                return false;

            return $jsonData[0];
        }
        return false;
    }

    public function getNumberOfViews($url){
        $data = $this->fetchAndParseData( $this->parseVideoID($url) );
        if($data)
            return number_format($data['statistics']['viewCount'], 0, '.',' ');
        return '?';
    }
}