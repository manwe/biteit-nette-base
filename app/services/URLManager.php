<?php
namespace App\Services;

use BiteIT\Orm;
use Nextras\Dbal\Connection;

class URLManager{

    /* @var Orm */
    protected $orm;

    public function __construct(Orm $orm){
        $this->orm = $orm;
    }

    public function generateURL($requestedURL, $id, $repositoryName){
        /** @var Connection $db */
        $db = $this->orm->data->getMapper()->getConnection();
        $row = $db->table('url')->where('objectId = ? AND objectRepository = ?', $id, $repositoryName)
                    ->select('url')->fetch();
        if($row)
        {
            if($requestedURL != $row->url){
                $this->orm->table('url')->where('url = ?', $row->url)->delete();
            }
            else
                return $requestedURL;
        }

        $urlExists = $this->orm->table('url')->where('url = ?', $requestedURL)->fetch();
        // url v db existuje
        if($urlExists)
        {
            // url patri stejnemu objektu, vse je ok
            if($urlExists->objectId == $id && $urlExists->objectRepository == $repositoryName)
                return $requestedURL;
            // url patri jinemu objektu, musim vygenerovat alternativni URL
            else
            {
                return $this->generateURL($requestedURL.'-'.$id, $id, $repositoryName);
            }
        }
        else
        {
            $this->orm->table('url')->insert(['url' => $requestedURL, 'objectId' => $id, 'objectRepository' => $repositoryName]);
            return $requestedURL;
        }
    }


}