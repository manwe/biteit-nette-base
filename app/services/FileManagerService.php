<?php
namespace App\Services;

use App\Model\Entity\Company;
use App\Model\Entity\File;
use App\Model\Entity\Order;
use App\Model\Entity\Task;
use App\Model\Entity\User;
use App\Model\Repository\FilesRepository;
use Nette\Application\Responses\FileResponse;
use Nette\Http\FileUpload;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Utils\Html;
use Nette\Utils\Image;
use Tracy\Debugger;
use Tracy\ILogger;

class FileManagerService
{
    const DISPLAY_BLOCKS = 1;
    const DISPLAY_LIST = 2;

    protected $uploadFolderInitialized = false;
    protected $basePath = null;
    protected $baseUrl = null;

    public $uploadFolder = '/upload/files/';
    public $rootFolder = __DIR__.'/../../www/';

    public $activeFolderId = null;
    public $thumbnailsProperties = [
        'width' => 450,
        'height' => 450
    ];

    /** @var FilesRepository */
    public $filesRepository;
    /** @var Session */
    public $session;

    /** @var null|File */
    public $activeFolder = null;
    /** @var null|Company */
    public $company = null;
    /** @var null|User */
    public $user = null;

    public $filesSizeLimit = 209715200;
    /** @var SessionSection */
    public $filter;

    public $extensionIcons = [
        "psd" => "icon-document-file-psd2",
        "zip" => "icon-document-file-zip2",
        "rar" => "icon-document-file-rar2",
        "pdf" => "icon-document-file-pdf2",
        "xml" => "icon-document-file-xml2",
        "xls" => "icon-document-file-excel2",
        "xlsx" => "icon-document-file-excel2"
    ];

    public function __construct(
        $basePath,
        FilesRepository $filesRepository,
        Session $session)
    {
        if(substr($basePath, -1, 1) != '/')
            $basePath .= '/';
        $this->basePath = $basePath;
        $this->filter = $session->getSection("fileManager");
        if(!isset($this->filter->displayType))
            $this->filter->displayType = static::DISPLAY_BLOCKS;

        $this->filesRepository = $filesRepository;
        $this->session = $session;
    }

    public function setBaseUrl($baseUrl){
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param \BiteIT\User $user
     * @return $this
     */
    public function setUser(\BiteIT\User $user){
        $this->user = $user;
//        $this->company = $user->getCompany();

        if(!$this->uploadFolderInitialized){
//            $this->uploadFolder .= $this->company->companyId.'/files/';
            $this->uploadFolder = implode("/", array_filter(explode("/", $this->uploadFolder)));
            $this->uploadFolder .= '/';

            $this->rootFolder .= $this->uploadFolder;
            if(!file_exists($this->rootFolder))
                mkdir($this->rootFolder, 0777, true);

            $this->uploadFolderInitialized = true;
        }

        return $this;
    }

    /**
     * @param File|null $folder
     * @return $this
     */
    public function setActiveFolder(File $folder = null){
        $this->activeFolder = $folder;
        return $this;
    }

    /**
     * @param $activeFolderId
     * @param array $extraConditions
     * @param string $ordering
     * @return \YetORM\EntityCollection|File[]
     */
    public function getFiles(/*$companyId, */$activeFolderId, $extraConditions = [], $ordering = 'system DESC, type DESC, ordering ASC'){
//        $conditions = ["companyId" => $companyId, "folderFileId" => $activeFolderId];
        $conditions = ["folderFileId" => $activeFolderId];
        $conditions = array_merge($conditions, $extraConditions);
        return $this->filesRepository->findBy($conditions)->orderBy($ordering);
    }

    /**
     * @param File|null $folder
     * @param array $extraConditions
     * @param string $ordering
     * @return \YetORM\EntityCollection|File[]
     */
    public function getChildrenFiles(File $folder = null, $extraConditions = [], $ordering = 'system DESC, type DESC, ordering ASC'){
        return $this->getFiles((isset($folder) ? $folder->fileId : null ), $extraConditions, $ordering);
    }

    /**
     * @param File $file
     * @return string
     */
    public function getFilePath(File $file){
        $path = $this->basePath.$this->uploadFolder.$file->path;
        return $path;
    }

    public function getImageUrl(File $file){
        $path = $this->baseUrl.$this->uploadFolder.$file->path;
        return $path;
    }

    public function getImageThumbUrl(File $file){
        $path = $this->baseUrl.$this->uploadFolder.'thumbs/'.$file->path;
        return $path;
    }

    /**
     * @param File $file
     * @param array $parents
     * @return array|NULL|\YetORM\Entity|File[]
     */
    public function getFolderParents(File $file, $parents = []){
        if($file->folderFileId) {
            /** @var File $parent */
            $parent = $this->filesRepository->getByID($file->folderFileId);
            if(isset($parent)) {

                $parents[] = $parent;
                return $this->getFolderParents($parent, $parents);
            }
        }
        return array_reverse($parents);
    }

    /**
     * @param File $folder
     * @return NULL|\YetORM\Entity|File
     */
    public function getFolderParent(File $folder){
        if($folder->folderFileId) {
            return $this->filesRepository->getByID($folder->folderFileId);
        }
        return null;
    }

    /**
     * @param File $file
     * @return bool
     */
    public function isFolderEmpty(File $file){
        if($file->isFolder()) {
            $rows = $this->filesRepository->getTable()->where(["folderFileId" => $file->fileId])->count("fileId");
            return $rows == 0;
        }
        return true;
    }

    /**
     * @param $basePath
     * @param $fileHash
     * @return FileResponse|null
     * @throws \Exception
     */
    public function getDownloadResponse($fileHash){
        $this->_validate();
        $file = $this->filesRepository->getByHash($fileHash);
        if($file)
            return new FileResponse($this->getFilePath($file));
        return null;
    }

    /**
     * @param $folderHash
     * @param $fileHash
     * @return bool
     */
    public function insertFileIntoFolder($folderHash, $fileHash){
        $file = $this->filesRepository->getByHash($fileHash);
        if($file){
            $folderId = $this->filesRepository->getIDByHash($folderHash);

            $file->folderFileId = $folderId;
            return $this->filesRepository->persist($file);
        } else {
            return false;
        }
    }

    /**
     * @param $fileHash
     * @param File|null $directFile
     * @return bool
     */
    public function deleteFile($fileHash, File $directFile = null){
        $this->_validate();
        if(isset($directFile) && $directFile instanceof File)
            $file = $directFile;
        else
            $file = $this->filesRepository->getByHash($fileHash);

        if(isset($file))
        {
            if($file->isFolder())
            {
                $childrenFiles = $this->filesRepository->getFilesByFolderId($file->fileId);
                foreach($childrenFiles as $childrenFile){
                    $this->deleteFile(null, $childrenFile);
                }
            }
            else
            {
                // soubor mazu pouze v pripade ze neni nikde jinde pouzity
                if($this->filesRepository->findBy(['path' => $file->path, 'fileId <> ?' => $file->fileId])->count('*') == 0)
                {
                    $realPath = $this->rootFolder . $file->path;
                    @unlink($realPath);
                    $realThumbPath = $this->rootFolder . 'thumbs/' . $file->path;
                    @unlink($realThumbPath);
                }
            }
            return $this->filesRepository->delete($file);
        }
        return false;
    }

    /**
     * @param FileUpload $file
     * @param File|null $folder
     * @return File
     * @throws \Exception
     */
    public function tryUploadAndSaveFile(FileUpload $file, File $folder = null){
        $this->_validate();
        $displayName = $file->getName();
        $displayName = pathinfo($displayName, PATHINFO_FILENAME);
        $fileName = $file->getSanitizedName();
        $baseName = pathinfo($fileName, PATHINFO_FILENAME);
        $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);
        $fileName = $baseName.'-'.time().'.'.$fileExt;

        $filePath = $fileName;
        try
        {
            $fullFilePath = $this->rootFolder.$filePath;
            $fullThumbPath = $this->rootFolder.'thumbs/'.$filePath;
            if(!file_exists($this->rootFolder.'thumbs/'))
                mkdir($this->rootFolder.'thumbs/');

            if(!$file->move($fullFilePath))
                throw new \Exception('Unable to save file to filesystem'.error_get_last());

            $orientation = null;
            if($file->isImage()) {

                $width = 400;
                $height = 400;

                $thumbProperties = $this->thumbnailsProperties;

                if(isset($thumbProperties['width']))
                    $width = $thumbProperties['width'];

                if(isset($thumbProperties['height']))
                    $height = $thumbProperties['height'];

                $image = Image::fromFile($fullFilePath);
                $image->resize($width, $height, Image::FIT | Image::SHRINK_ONLY);
                $image->save($fullThumbPath);
                $orientation = ($image->getWidth() > $image->getHeight() ? FilesRepository::ORIENTATION_LANDSCAPE : FilesRepository::ORIENTATION_PORTRAIT);
            }

            $dbFile = new File();
//            $dbFile->companyId = $this->company->companyId;
            $dbFile->folderFileId = $folder ? $folder->fileId : null;
            $dbFile->ownerId = $this->user->id;
            $dbFile->name = $displayName;
            $dbFile->path = $filePath;
            $dbFile->type = FilesRepository::TYPE_FILE;
            $dbFile->size = filesize($fullFilePath);
            $dbFile->isImage = ($file->isImage() ? 1 : 0 );
            $dbFile->orientation = $orientation;
            $dbFile->dateCreated = new \DateTime();
            $dbFile->deleted = 0;
            $this->filesRepository->persist($dbFile);

            return $dbFile;

        } catch (\Exception $e) {
            bd($e);
            Debugger::log("Nepovedlo se nahrát soubor: $fileName. ".$e->getMessage(), ILogger::ERROR);
            throw new \Exception("Nepovedlo se nahrát soubor", $e->getCode(), $e);
        }
    }


    protected function _getSystemFolder($hash, $desiredName, $isSystem, Company $company, User $user, File $folderParent = null){
        $dbFile = $this->filesRepository->getByHash($hash);
        if(!$dbFile) {
            $dbFile = new File();
            $dbFile->name = $desiredName;
            $dbFile->companyId = $company->companyId;
            if(isset($folderParent))
                $dbFile->folderFileId = $folderParent->fileId;
            $dbFile->ownerId = $user->id;
            $dbFile->path = '';
            $dbFile->type = FilesRepository::TYPE_FOLDER;
            $dbFile->isImage = 0;
            $dbFile->orientation = null;
            $dbFile->dateCreated = new \DateTime();
            $dbFile->hash = $hash;
            if($isSystem)
                $dbFile->system = 1;
            $this->filesRepository->persist($dbFile, false);
            return $dbFile;
        } else {
            $dbFile->name = $desiredName;
            if($isSystem)
                $dbFile->system = 1;
            $this->filesRepository->persist($dbFile, false);
        }
        return $dbFile;
    }

    /**
     * @return int
     */
    public function calculateCompanyFolderSize(){
        $folderDir = $this->rootFolder;
        return $this->_calculateFolderSize($folderDir);
    }

    /**
     * @param $dir
     * @return int
     */
    protected function _calculateFolderSize($dir){
        $size = 0;
        foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : $this->_calculateFolderSize($each);
        }
        return $size;
    }

    protected function _validate(){
//        if(!isset($this->company))
//            throw new \Exception("Please set user");
        if(!isset($this->user))
            throw new \Exception("Please set user");
    }

    public function renderFullImage(File $file){
       $filePath = $this->getImageUrl($file);
       $img = Html::el("img");
       $img->setAttribute("src", $filePath);
       return $img;
    }
}