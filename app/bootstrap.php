<?php
mb_internal_encoding('utf-8');
require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode( ['78.157.162.75', '78.157.167.98'] );
$configurator->enableDebugger(__DIR__ . '/../log');

\Tracy\Debugger::$strictMode = FALSE;
\Tracy\Debugger::$maxDepth = 10;

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
if( strpos($_SERVER['HTTP_HOST'], 'localhost') !== false || !file_exists(__DIR__ . '/config/config.production.neon'))
    $configurator->addConfig(__DIR__ . '/config/config.local.neon');
else
    $configurator->addConfig(__DIR__ . '/config/config.production.neon');


$container = $configurator->createContainer();

require __DIR__ . '/shortcuts.php';

return $container;
