<?php

namespace BiteIT;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class DragAndDropUpload extends Control {

    public $onUpload;

    public $text = 'Drop files here to upload...';
    public $buttonText = 'or click here...';
    public $inputName = 'file';
    public $isAjax = true;
    public $extraInputs = null;

    public function createComponentDragAndDropForm(){
        $form = new Form();
//        $form->addUpload($this->inputName);
        $form->onSuccess[] = [$this, 'processUpload'];
        return $form;
    }

    public function processUpload($form, $values){
        call_user_func($this->onUpload, $form, $form->getHttpData());
    }

    public function render(){
        $this->template->extraInputs = $this->extraInputs;
        $this->template->isAjax = $this->isAjax;
        $this->template->text = $this->text;
        $this->template->buttonText = $this->buttonText;
        $this->template->inputName = $this->inputName;
        $this->template->render(__DIR__.'/DragAndDropUpload.latte');
    }

}