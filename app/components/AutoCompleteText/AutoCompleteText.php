<?php
namespace Cerebro;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class AutoCompleteText extends Control{

    protected $inputName = 'autocompleteText';
    protected $inputLabel = 'Start typing in...';
    protected $itemIdColumn = 'id';
    protected $itemTextColumn = 'text';
    protected $dataLoadCallable = null;
    protected $itemClickedCallable = null;
    protected $items = [];

    public function createComponentAutoCompleteForm(){
        $f = new Form();
        $f->addText($this->inputName, $this->inputLabel);
        $f->onSuccess[] = [$this, 'processAutoComplete'];
        return $f;
    }

    public function processAutoComplete(Form $form, $values){
        if(!is_callable($this->dataLoadCallable)) {
            $this->dataLoadCallable = [$this, 'loadRandomStuff'];
        }
        $this->items = call_user_func($this->dataLoadCallable, $values[$this->inputName]);
        $this->redrawControl('itemList');
    }

    public function loadRandomStuff($value){
        $return = [];
        $x = 0;

        foreach(str_split($value) as $letter)
        {
            $obj = new \stdClass();
            $obj->{$this->itemTextColumn} = 'Letter "'.$letter.'"';
            $obj->{$this->itemIdColumn} = ++$x;
            $return[] = $obj;
        }
        return $return;
    }

    public function render(){
        $this->template->inputName = $this->inputName;
        $this->template->inputLabel = $this->inputLabel;
        $this->template->itemIdColumn  = $this->itemIdColumn;
        $this->template->itemTextColumn  = $this->itemTextColumn;
        $this->template->items = $this->items;
        $this->template->setFile(__DIR__.'/AutoCompleteText.latte');
        $this->template->render();
    }

    public function handleItemClicked($id){
        $this->items = [];
        $this->redrawControl('itemList');
        $this->redrawControl('input');
        call_user_func($this->itemClickedCallable, $id);
    }


    public function setItemIdColumn($idColumnName){
        $this->itemIdColumn = $idColumnName;
    }
    public function setItemTextColumn($textColumnName){
        $this->itemTextColumn = $textColumnName;
    }
    public function setDataLoadCallable($callable){
        if(is_callable($callable))
            $this->dataLoadCallable = $callable;
        else
            throw new \Exception('Passed parameter '.$callable.' is not callable');
    }
    public function setItemClickedCallable($callable){
        if(is_callable($callable))
            $this->itemClickedCallable = $callable;
        else
            throw new \Exception('Passed parameter '.$callable.' is not callable');
    }
    public function setInputName($inputName){
        $this->inputName = $inputName;
    }
    public function setInputLabel($inputLabel){
        $this->inputLabel = $inputLabel;
    }

}