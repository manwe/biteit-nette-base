<?php

namespace App\FrontendModule\Presenters;

use App\Model\Entities\Data;
use App\Presenters\PublicPresenter;

class ViewPresenter extends PublicPresenter
{


    public function actionDefault($url = '')
    {
        if($url)
        {
            $content = $this->_fetchContentByURL($url);
            $this->template->content = $content;
            if($content instanceof Data){
                $this->setView($content->type);
            }
        }
    }

    protected function _fetchContentByURL($url){
        $url = $this->orm->urls->getBy(['url' => $url]);
        if($url)
        {
            try{
                $repo = $this->orm->getRepository($url->objectRepository);
                if($repo instanceof \Nextras\Orm\Repository\Repository)
                {
                    return $repo->getByID($url->objectId);
                }
            }
            catch (\Exception $e){
                bd($e);
            }
        }
    }


}
