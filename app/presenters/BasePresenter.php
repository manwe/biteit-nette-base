<?php

namespace App\Presenters;

use App\Model\Repository\DataRepository;
use App\Model\Repository\GuideClipRepository;
use App\Model\Repository\GuideRepository;
use App\Model\Repository\SiriusInstanceRepository;
use App\Model\Repository\UserPermissionRepository;
use App\Model\Repository\UserRepository;
use App\Services\URLManager;
use BiteIT\Orm;
use BiteIT\SiriusInstallationForm;
use Nette;
use Nextras\Dbal\Utils\DateTimeImmutable;
use Tracy\ILogger;


abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var URLManager @inject */
    public $urlManager;

    /** @var Orm @inject */
    public $orm;

    /** @var Nette\Http\Request @inject */
    public $request;

    public $adminEmail;
    public $httpRequest;
    public $presenterName = '';

    protected $menuItems = [];

    public function startup()
    {
        parent::startup();

        $this->httpRequest = $this->context->getByType('Nette\Http\Request');
        $this->adminEmail = $this->context->getParameters()['adminEmail'];
        $this->template->addFilter('datum', function($s){
            $t = strtotime($s);
            if($t > 0)
                return strftime("%d.%m.%Y %H:%M:%S", $t);
            return '-';
        });

        $this->template->addFilter('flashIconType', function($type){
            switch($type){
                case 'error':
                case 'danger':
                    return 'warning';

                case 'ok':
                case 'success':
                    return 'check';

                default:

                    return 'info';
            }
        });

        $this->template->addFilter('formattedDate', function(DateTimeImmutable $date, $format = 'd.m.Y H:i:s', $returnOnInvalid = ''){
            $ts = $date->getTimestamp();
            if($ts > 0)
                return date($format, $date->getTimestamp());
            return $returnOnInvalid;
        });

        $this->template->addFilter('formatPrice', function($price, $precision = 0, $decPoint = ',', $thSeparator = ' '){
            return number_format(round($price, $precision), $precision, $decPoint, $thSeparator);
        });
    }

    public function isBackend(){
        return false;
    }

    public function beforeRender()
    {
        parent::beforeRender();

    }

    public function getProjectName(){
        return $this->context->parameters['projectName'];
    }

    public function getMenuItems(){
        return $this->context->parameters['menuItems'];
    }

    public function setModalHTML($innerHTML, $autoRedraw = true){
        $html = '
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                '.$innerHTML.'
                </div>
            </div>
        </div>';

        $this->template->modalHTML = $html;

        if($autoRedraw)
            $this->redrawControl('modal');
    }

    public function unsetModalHTML(){
        $this->template->modalHTML = '<script type="text/javascript"> $(\'.modal-backdrop\').fadeOut(); </script>';
        $this->redrawControl('modal');
    }

    /**
     * @param $message
     * @param string $priority
     */
    public function log($message, $priority = ILogger::INFO){
        $referent = 'System';
        if($this->getUser()->isLoggedIn())
            $referent = $this->getUserEntity()->email;
        trLog("$referent - $message", $priority);
    }

    public function isDev(){
        return $_SERVER['HTTP_HOST'] == 'localhost';
    }

    /**
     * @param bool $throw
     * @return Nette\Application\UI\Presenter|BasePresenter
     */
    public function getPresenter($throw = TRUE){
        return parent::getPresenter($throw);
    }

    public function flashMessage($message, $type = 'ok')
    {
        $ret = parent::flashMessage($message, $type);
        $this->redrawControl('flashes');
        return $ret;
    }

}
