<?php

namespace App\Presenters;

use App\Model\CerebroAuthorizator;
use App\Model\Repository\FilesRepository;
use BiteIT\ProcessBar;
use BiteIT\User;
use Nette\Http\SessionSection;

abstract class PrivatePresenter extends BasePresenter
{
    /* @var $userEntity User */
    public $userEntity;

    /** @var SessionSection */
    protected $globalSession;

    const UPLOAD_DIR = __DIR__.'/../../www/upload/';


    public function startup()
    {
        parent::startup();

        if($this->user->isLoggedIn())
        {
            $this->userEntity = $this->orm->users->getByID( $this->user->getId() );
            $this->template->userEntity = $this->userEntity;

        }
        else
        {
            if($this->user->getLogoutReason())
            {
            }
            $this->redirect('Sign:default');
        }

        $this->globalSession = $this->getSession()->getSection('global');
    }

    /**
     * @return \App\Model\Entities\User|null
     */
    public function getUserEntity(){
        if($this->getUser()->isLoggedIn())
            return $this->userEntity;
        return null;
    }
    public function isBackend()
    {
        return true;
    }
}
