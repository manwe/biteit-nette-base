<?php

namespace Backend\Components;

use App\Model\Entities\User;
use App\Model\Repository\UserRepository;
use BiteIT\Orm;
use Nette\Application\UI\Control;
use Nette\Security\Passwords;

class UserForm extends Control {

    /** @var Orm */
    protected $orm;
    /** @var \BiteIT\User */
    protected $userEntity;



    public function render($params = null){
        $this->userEntity = $this->orm->users->getByID($params['id']);
        $this->template->userEntity = $this->getPresenter()->userEntity;
        $this->template->render(__DIR__.'/UserForm.latte');
    }

    public function createComponentUserForm(){
        $form = new \ExtForm();

        $form->addEmail('email', 'Přihlašovací e-mail:')
            ->setAttribute("placeholder", "Přihlašovací e-mail:")
            ->setRequired();

        $form->addPassword('password', 'Heslo:')
            ->setAttribute("placeholder", "Heslo:");

        $form->addText('firstname', 'Jméno:')
            ->setAttribute("placeholder", "Jméno:");

        $form->addText('surname', 'Příjmení:')
            ->setAttribute("placeholder", "Příjmení:");

        if ($this->getPresenter()->userEntity->superadmin)
        {
            $form->addCheckbox('superadmin', ' Superadmin')
                ->setAttribute('checked', false);
        }

        $form->addHidden('id');

        if($this->userEntity)
        {
            $form->setDefaults([
                'email' => $this->userEntity->email,
                'password' => $this->userEntity->password,
                'firstname' => $this->userEntity->firstname,
                'surname' => $this->userEntity->surname,
                'id' => $this->userEntity->id,
                'superadmin' => $this->userEntity->superadmin
            ]);
        }

        $form->onSuccess[] = [$this, 'userFormSucceeded'];
        return $form;
    }

    public function userFormSucceeded($form, $values)
    {

        $destination = null;
        $args = null;
        bd($values);

        if($values['id'] AND $this->userEntity = $this->orm->users->getBy(['id' => $values['id']]))
        {
            $destination = "";
            $args = $this->userEntity->id;

            $values['password'] = Passwords::hash($values['password']);

            try {

                bd($this->userEntity);
                $this->userEntity->load($values);
                $userEntity =$this->userEntity;
                $this->orm->users->persistAndFlush($userEntity);
                $this->presenter->flashMessage("Uživatel byl uložen", 'ok');
                $this->getPresenter()->log("upravil uživatele: {$userEntity->id} - {$userEntity->email}");

            } catch (\Exception $e) {
                $this->presenter->flashMessage($e->getMessage(), 'error');
            }
        }
        else
        {
            try {

                $this->orm->users->createUser($values['email'], $values['password'], $values);
                $this->presenter->flashMessage("Uživatel byl vytvořen", 'ok');
                $destination = "";
                $this->getPresenter()->log("vytvořil uživatele: {$values['email']}");

            } catch (\Exception $e) {
                $this->presenter->flashMessage($e->getMessage(), 'error');
//                $destination = "new";
            }
        }

        $this->presenter->redirect("Settings:users");
    }

    /**
     * @param Orm $ur
     */
    public function setOrm(Orm $ur){
        $this->orm = $ur;
    }

}