<?php

namespace App\BackendModule\Presenters;

use App\Presenters\PublicPresenter;
use Nette;

class SignPresenter extends PublicPresenter
{
	protected function createComponentSignInForm()
	{
		$form = new Nette\Application\UI\Form();

        $form->addEmail('email', 'Přihlašovací e-mail:')
                ->setAttribute("placeholder", "Přihlašovací e-mail:")
                ->setRequired("Zadejte prosím přihlašovací e-mail");

        $form->addPassword("password")
            ->setAttribute("placeholder", "Heslo")
            ->setRequired("Vyplňtě prosím heslo");

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];
        return $form;
	}


	public function actionOut()
	{
		$this->getUser()->logout();
		$this->redirect("Sign:default");
	}

    public function signInFormSucceeded(Nette\Application\UI\Form $form, $values)
    {
        if(isset($values['email']) && isset($values['password']))
        {
            try {
                $this->user->login($values['email'], $values['password']);

            }
            catch (\Exception $e){
                $this->flashMessage($e->getMessage(), 'error');
            }

            if($this->user->isLoggedIn()) {
                $this->flashMessage('Přihlášení bylo úspěšné.', 'ok');
                $this->redirect('Dashboard:');
            }
            $this->redirect('Sign:default');
        }
    }
}
