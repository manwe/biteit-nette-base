<?php

namespace App\BackendModule\Presenters;

use App\Model\Entities\User;
use App\Presenters\PrivatePresenter;
use Backend\Components\UserForm;

class SettingsPresenter extends PrivatePresenter
{


    public function actionUsers($id = null){
        $conds = [];

        if(!$this->userEntity->superadmin)
            $conds = ['superadmin' => 0];

        $this->template->users = $this->orm->users->findBy( $conds );

    }

    public function actionUserEdit($id){
        $this->template->userId = $id;
        $this->setView('edit');
    }
    public function actionUserNew(){
        $this->setView('edit');
    }

    public function handleBanAction($id){
        /** @var User $user */
        $user = $this->orm->users->getByID($id);
        $user->active = !$user->active;
        $this->orm->users->persist($user);

        $this->flashMessage('Uživatel úspěšně '.($user->active ? 'odblokován' : 'zablokován').'.', 'success');
        $this->redrawControl('users');
    }

    public function createComponentUserForm(){
        $formCreator = new UserForm();
        $formCreator->setOrm($this->orm);
        return $formCreator;
    }
}
