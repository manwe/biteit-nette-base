<?php

function bd(){
    foreach(func_get_args() as $v){
        \Tracy\Debugger::barDump($v, basename(debug_backtrace()[0]['file']).":".debug_backtrace()[0]['line'] );
    }
}

function vd(){
    foreach(func_get_args() as $v){
        \Tracy\Debugger::dump($v);
    }
}

function trLog($message, $priority = \Tracy\ILogger::INFO){
    \Tracy\Debugger::log($message, $priority);
}

function rrmdir($dir, $excludedFolders = []) {
    if (is_dir($dir) AND !in_array($dir, $excludedFolders)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if(!in_array($object, $excludedFolders) OR !in_array($dir."/".$object, $excludedFolders))
                {
                    if (is_dir($dir."/".$object))
                        rrmdir($dir."/".$object, $excludedFolders);
                    else
                        unlink($dir."/".$object);
                }
            }
        }
        rmdir($dir);
    }
}