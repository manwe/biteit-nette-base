-- Adminer 4.3.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `data`;
CREATE TABLE `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_parent` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `ordering` int(11) NOT NULL,
  `values` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `data_parent` (`data_parent`),
  CONSTRAINT `data_ibfk_1` FOREIGN KEY (`data_parent`) REFERENCES `data` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` varchar(50) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `url`;
CREATE TABLE `url` (
  `url` varchar(200) NOT NULL,
  `object_repository` varchar(100) NOT NULL,
  `object_id` int(11) NOT NULL,
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `date_logged` datetime NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `superadmin` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`userId`, `email`, `password`, `firstname`, `surname`, `date_logged`, `active`, `superadmin`) VALUES
(18,	'radek@biteit.cz',	'$2y$10$UUxR7XejcKLimwhCIFDovOt0SO5THGYfckegeknFaqKl8chHbO36W',	'Radek',	'Boszczyk',	'2018-06-27 14:34:17',	1,	1);

-- 2018-06-27 12:35:05
