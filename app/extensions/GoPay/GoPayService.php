<?php
/**
 * User: Manwe
 */
namespace BiteIT;

use GoPay\Api;
use GoPay\Definition\Language;
use GoPay\Definition\TokenScope;
use GoPay\GoPay;
use Nette\Application\UI\Presenter;

class GoPayService
{
    protected   $gopayId,
                $gopayClientId,
                $gopayClientSecret,
                $gopayIsLive = false;

    public function __construct($gopayId, $gopayClientId, $gopayClientSecret, $gopayIsLive)
    {
        $this->gopayClientId = $gopayClientId;
        $this->gopayId = $gopayId;
        $this->gopayClientSecret = $gopayClientSecret;
        $this->gopayIsLive = $gopayIsLive;
    }

    public function createPayment(Presenter $presenter, Order $order){
        $oi = null;
        foreach ($order->items as $i){
            $oi = $i;
            break;
        }
        $gopayItems = [];
        $gopayItems[] = ['type' => 'ITEM',
            'product_url' => $presenter->template->baseUrl.$presenter->link('View:default', $oi->term->trip->url),
            'name' => $oi->term->trip->name,
            'amount' => (int) round($oi->price*100),
            'count' => (int) 1,
            'ean' => 'T'.$oi->term->trip->id,
            'vat_rate' => 21, ];

        $gopay = Api::payments([
            'goid' => $this->gopayId,
            'clientId' => $this->gopayClientId,
            'clientSecret' => $this->gopayClientSecret,
            'isProductionMode' => $this->gopayIsLive,
            'scope' => TokenScope::CREATE_PAYMENT,
            'language' => Language::CZECH,
            'timeout' => 10
        ]);


        $payment = [
            'target' => [
                'type' => 'ACCOUNT',
                'goid' => $this->gopayId
            ],
            'amount' => round($order->total * 100),
            'currency' => 'CZK',
            'order_number' => $order->orderNumber,
            'items' => $gopayItems,
            'callback' => [
                'return_url' => $presenter->template->baseUrl.$presenter->link('Order:cart', ['step' => 3]),
                'notification_url' => $presenter->template->baseUrl.$presenter->link('Order:notifyGopay')
            ]
        ];


        $response = $gopay->createPayment($payment);
        if($response->hasSucceed())
        {
            return $response->json;
        }
        else{
            bd($response->json);
            return false;
        }
    }
}