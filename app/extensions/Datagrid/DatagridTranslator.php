<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 09.06.2017
 * Time: 17:34
 */

namespace App\Model;

use Ublaboo\DataGrid\Localization\SimpleTranslator;

class DatagridTranslator extends SimpleTranslator
{
    public function __construct($dictionary = NULL)
    {
        $default = [
            'ublaboo_datagrid.no_item_found_reset' => 'Žádné položky nenalezeny. Filtr můžete vynulovat',
            'ublaboo_datagrid.no_item_found' => 'Žádné položky nenalezeny.',
            'ublaboo_datagrid.here' => 'zde',
            'ublaboo_datagrid.items' => 'Položky',
            'ublaboo_datagrid.all' => 'všechny',
            'ublaboo_datagrid.from' => 'z',
            'ublaboo_datagrid.reset_filter' => 'Resetovat filtr',
            'ublaboo_datagrid.group_actions' => 'Hromadné akce',
            'ublaboo_datagrid.show_all_columns' => 'Zobrazit všechny sloupce',
            'ublaboo_datagrid.hide_column' => 'Skrýt sloupec',
            'ublaboo_datagrid.action' => 'Akce',
            'ublaboo_datagrid.previous' => 'Předchozí',
            'ublaboo_datagrid.next' => 'Další',
            'ublaboo_datagrid.choose' => 'Vyberte',
            'ublaboo_datagrid.execute' => 'Provést',
            'ublaboo_datagrid.per_page_submit' => 'Změnit',
            'ublaboo_datagrid.cancel' => 'Zrušit',
            'ublaboo_datagrid.save' => 'Uložit',
            'ublaboo_datagrid.edit' => 'Upravit',
        ];

        if(isset($dictionary) AND is_array($dictionary))
            $dictionary = array_merge($default, $dictionary);
        else
            $dictionary = $default;

        parent::__construct($dictionary);
    }
}