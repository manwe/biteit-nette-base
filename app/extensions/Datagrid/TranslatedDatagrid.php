<?php
namespace BiteIT;

use App\Model\DatagridTranslator;
use Ublaboo\DataGrid\DataGrid;

class TranslatedDatagrid extends DataGrid
{
    public function __construct($parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->setTranslator( new DatagridTranslator() );
    }
}