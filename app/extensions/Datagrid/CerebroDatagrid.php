<?php
namespace Cerebro;

use App\Model\CerebroDataGridTranslator;
use Ublaboo\DataGrid\DataGrid;

class CerebroDatagrid extends DataGrid
{
    public function __construct($parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->setTranslator( new CerebroDataGridTranslator() );
    }
}