<?php
class MyMacros extends \Latte\Macros\MacroSet {

    public static function install(\Latte\Compiler $compiler) {
        $set = new static($compiler);

        $set->addMacro(
            'selected-if', null, null, array($set, 'checkIfIsSelected')
        );
    }

    /**
     * @param \Latte\MacroNode $node
     * @param \Latte\PhpWriter $writer
     * @return string
     * @throws Exception
     */
    public function checkIfIsSelected(\Latte\MacroNode $node, \Latte\PhpWriter $writer) {
        if($node->htmlNode->name == 'input')
            return "if(".$node->args.") echo ' checked '";
        else
            return "if(".$node->args.") echo ' selected '";
    }
}