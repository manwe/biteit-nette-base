<?php

class ExtForm extends \Nette\Application\UI\Form
{
    public function loadFromEntity(\YetORM\Entity $entity){
        $defaults = [];
        foreach($this->getValues() as $name => $value){
            $defaults[$name] = $entity->$name;
        }
        $this->setDefaults($defaults);
    }

    /**
     * @param $name
     * @param string $format
     * @throws Exception
     */
    public function changeValueDateFormat($name, $format = 'd.m.Y H:i:s'){
        if(isset($this[$name]))
        {
            $input = $this[$name];
            if($input instanceof \Nette\Forms\Controls\TextInput)
            {
                $value = $input->getValue();
                if($value instanceof DateTime)
                    $value = date($format, $value->getTimestamp());
                else
                    $value = date($format, strtotime($value));

                $this->setDefaults([$name => $value]);
            }
            else
                throw new Exception("$name is not valid input of this form");
        }
        else
            throw new Exception("$name does not exist in this form");
    }
}