<?php
namespace App\Model;

use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class MailSender
{
    protected $mail;
    protected $adminEmail = '';

    public function __construct($adminEmail){
        $this->mail = new Message();
        $this->adminEmail = $adminEmail;
    }

    public function setFrom($from, $name = null){
        return $this->mail->setFrom($from, $name);
    }

    public function addBcc($bcc){
        foreach((array) $bcc as $bc)
            $this->mail->addBcc($bc);
    }

    public function addTo($to){
        foreach((array) $to as $t)
            $this->mail->addTo($t);
    }

    public function addAttachment($file, $content = null, $contentType = null){
        return $this->mail->addAttachment($file, $content, $contentType);
    }

    public function fromAdmin(){
        $this->setFrom( $this->adminEmail );
    }

    public function toAdmin(){
        $this->addTo($this->adminEmail);
    }

    public function setHTML($html, $basePath = null){
        return $this->mail->setHtmlBody($html, $basePath);
    }

    public function setBody($body){
        return $this->mail->setBody($body);
    }

    public function setSubject($subject){
        return $this->mail->setSubject($subject);
    }

    /**
     * @return bool
     */
    public function send(){
        $mailer = new SendmailMailer();
        try{
            $mailer->send($this->mail);
            return true;
        }
        catch(\Exception $e){
            return false;
        }
    }

}