<?php
/**
 * User: Manwe
 */

class HTMLFactory
{
    public static function SwitchButton($name, $state, $callback = null)
    {
        $id = 'sb_' . mt_rand(10000, 99999);

        $html = '
            <span class="switch-button" id="' . $id . '">
                <div class="slider">
                    <div class="side on-side" style="width: ' . ($state == 0 ? '1' : '90') . '%"><span class="icon-checkmark"></span></div>
                    <div class="drag"></div>
                    <div class="side off-side"><span class="icon-cross"></span></div>
                </div>
                <input name="' . $name . '" type="hidden" value="' . ($state ? 1 : 0) . '">

                <script type="text/javascript">
                    $(\'#' . $id . '\').click(function(){
                        if( !$(this).hasClass(\'moving\') )
                        {
                            $(this).addClass(\'moving\');
                            var cval = $(this).find(\'input[type="hidden"]\').val();
                            if(cval == 0)
                            {
                                console.log(\'turning on\');
                                $(this).find(\'.on-side\').animate({width: \'90%\'}, 100, function(){
                                    $(\'#' . $id . '\').removeClass(\'moving\');
                                });
                            }
                            else
                            {
                                console.log(\'turning off\');
                                $(this).find(\'.on-side\').animate({width: \'1%\'}, 100, function(){
                                    $(\'#' . $id . '\').removeClass(\'moving\');
                                });
                            }
                            
                            $(this).find(\'input[type="hidden"]\').val( 1 - cval );
                            
                            ' . ($callback ? $callback . '(this,\'' . $name . '\')' : '') . '
                        }
                    });
                </script>
            </span>
        ';

        return $html;
    }

}