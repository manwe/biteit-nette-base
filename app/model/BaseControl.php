<?php

class BaseControl extends \Nette\Application\UI\Control
{
    /**
     * @param bool $throw
     * @return \Nette\Application\UI\Presenter|NULL|\App\Presenters\BasePresenter
     */
    public function getPresenter($throw = TRUE){
        return parent::getPresenter($throw);
    }

    /**
     * @param $type
     * @return object
     */
    public function getServiceByType($type){
        return $this->getPresenter()->context->getByType($type);
    }

}