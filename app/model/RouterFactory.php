<?php

namespace App\Model;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
	    $router = new RouteList();

		$routerList = new RouteList('Backend');
        $routerList[] = new Route('spravce/<presenter>/<action>[/<id>]', 'Sign:default');
        $router[] = $routerList;

        $routerList = new RouteList('Frontend');
		$routerList[] = new Route('[<url>]', 'View:default');
		$router[] = $routerList;

		return $router;
	}

}
