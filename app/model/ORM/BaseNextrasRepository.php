<?php
namespace BiteIT;

use Nette\Utils\DateTime;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Mapper\IMapper;
use Nextras\Orm\Model\IModel;
use Nextras\Orm\Repository\Repository;

abstract class BaseNextrasRepository extends Repository {

    public function persist(IEntity $entity, bool $withCascade = true)
    {
        if($entity->getMetadata()->hasProperty('dateCreated') && !$entity->isPersisted()){
            $entity->dateCreated = new DateTime();
        }
        if($entity->getMetadata()->hasProperty('dateUpdated')){
            if($entity->isPersisted())
                $entity->dateUpdated = new DateTime();
            else
                $entity->dateUpdated = null;
        }

        return parent::persist($entity, $withCascade);
    }

    /**
     * @param $id
     * @return false|BaseNextrasEntity|null
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param array $conds
     * @return mixed|BaseNextrasEntity|null
     */
    public function getBy(array $conds)
    {
        return parent::getBy($conds);
    }

    /**
     * @return Orm
     */
    public function getModel(): IModel
    {
        return parent::getModel();
    }

    /**
     * @return \BaseNextrasMapper
     */
    public function getMapper(): IMapper
    {
        return parent::getMapper();
    }

}