<?php
namespace BiteIT;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Repository\IRepository;


class BaseNextrasEntity extends Entity{

    public function load($data){
        $array = is_array($data);
        foreach ($this->getMetadata()->getProperties() as $property)
        {
            if($array && !isset($data[$property->name]))
                continue;
            else if(!$array && !isset($data->{$property->name}))
                continue;

            $value = $array ? $data[$property->name] : $data->{$property->name};

            try
            {
                $this->setValue($property->name, $value);
            }
            catch (\Exception $exception)
            {
                continue;
            }
        }
    }

    /**
     * @return BaseNextrasRepository
     */
    public function getRepository(): IRepository
    {
        return parent::getRepository();
    }


}