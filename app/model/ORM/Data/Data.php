<?php

namespace App\Model\Entities;

use App\Model\Entity\ExtEntity;
use BiteIT\BaseNextrasEntity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * @property int $id {primary}
 * @property Data|null $dataParent          {m:1 Data::$children}
 * @property OneHasMany|Data[] $children    {1:m Data::$dataParent}
 * @property string $type
 * @property string $name
 * @property int $ordering {default 0}
 * @property string $values {default ''}
 * @property int $active {default 1}
 * @property \DateTimeImmutable|null $dateCreated
 * @property \DateTimeImmutable|null $dateUpdated
 */
class Data extends BaseNextrasEntity
{

    public $unpackedData = null;

    public function & __get($name)
    {
        if (($isDynamicValue = (substr($name, 0, 1) == '_')) || $name != 'values') {
            if ($isDynamicValue)
                $name = substr($name, 1);

            if ($this->unpackedData === null)
                $this->unpackedData = json_decode($this->values, true);

            if (isset($this->unpackedData[$name]))
                return $this->unpackedData[$name];

            if ($isDynamicValue) {
                $value = null;
                return $value;
            }
        }
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        $isDynamicValue = (substr($name, 0, 1) == '_');
        if(!$isDynamicValue)
            return parent::__set($name,$value);

        $name = substr($name, 1);

        if ($this->unpackedData === null && $this->isPersisted())
            $this->unpackedData = json_decode($this->values, true);

        $this->unpackedData[$name] = $value;
    }

}