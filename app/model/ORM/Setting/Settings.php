<?php

namespace App\Model\Entities;

use App\Model\Entity\ExtEntity;
use BiteIT\BaseNextrasEntity;

/**
 * @property string $id {primary}
 * @property string $data {default ''}
 */
class Settings extends BaseNextrasEntity
{
    public $unpackedData = null;
    public $triedUnpacking = false;

    public function set($key, $value){
        $this->unpackData();
        $this->unpackedData[$key] = $value;

        $this->data = json_encode($this->unpackedData);
    }

    public function get($key, $defaultValue = ''){
        $this->unpackData();

        if(isset($this->unpackedData[$key]))
            return $this->unpackedData[$key];
        return $defaultValue;
    }

    protected function unpackData(){
        if($this->unpackedData === null && !$this->triedUnpacking){
            $this->triedUnpacking = true;
            $this->unpackedData = json_decode($this->data, true);
        }
    }


}