<?php
namespace BiteIT;

use App\Model\Entities\Settings;
use Nextras\Orm\Entity\IEntity;


class SettingsRepository extends BaseNextrasRepository {

    public static function getEntityClassNames(): array
    {
        return [Settings::class];
    }

    public function persist(IEntity $entity, bool $withCascade = true)
    {
        return parent::persist($entity, $withCascade);
    }

    /**
     * @param $section
     * @return Settings
     */
    public function getSettings($section){
        if(!$this->getById($section)){
            $settings = new Settings();
            $settings->id = $section;
            $settings->data = '';
            $this->persistAndFlush($settings);
        }
        return $this->getBy(['id' => $section]);
    }

}