<?php
namespace BiteIT;

use Nextras\Orm\Model\Model;

/**
 * Model ORM
 *
 * @property-read UserRepository $users
 * @property-read DataRepository $data
 * @property-read URLRepository $urls
 * @property-read SettingsRepository $settings
 */
class Orm extends Model {


}