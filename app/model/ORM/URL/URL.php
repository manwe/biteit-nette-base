<?php

namespace App\Model\Entities;

use App\Model\Entity\ExtEntity;
use BiteIT\BaseNextrasEntity;

/**
 * @property string $id {primary-proxy}
 * @property string $url {primary}
 * @property string $objectRepository
 * @property int $objectId
 */
class URL extends BaseNextrasEntity
{


}