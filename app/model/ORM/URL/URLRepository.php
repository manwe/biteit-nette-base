<?php
namespace BiteIT;

use App\Model\Entities\URL;
use Nette\Utils\Strings;
use Nextras\Orm\Entity\IEntity;


class URLRepository extends BaseNextrasRepository {

    /**
     * Returns possible entity class names for current repository.
     * @return string[]
     */
    public static function getEntityClassNames(): array
    {
        return [URL::class];
    }

    public function generateUniqueURL($requestedURL, IEntity $e){
        $requestedURL = Strings::webalize($requestedURL);
        $repositoryClass = get_class($e->getRepository());
        $objectId = $e->getPersistedId();

        $item = $this->getBy(['objectId' => $objectId, 'objectRepository' => $repositoryClass ]);
        if($item)
        {
            if($requestedURL != $item->url)
                $this->removeAndFlush($item);
            else
                return $requestedURL;
        }

        $matchingUrlItem = $this->getBy(['url' => $requestedURL]);
        if($matchingUrlItem)
        {
            if($matchingUrlItem->objectId == $objectId && $matchingUrlItem->objectRepository == $repositoryClass)
                return $requestedURL;
            else
                return $this->generateUniqueURL($requestedURL.'-'.$objectId, $e);
        }
        else
        {
            $url = new URL();
            $url->url = $requestedURL;
            $url->objectId = $objectId;
            $url->objectRepository = $repositoryClass;
            $this->persistAndFlush($url);
            return $requestedURL;
        }
    }

    /**
     * @param array $conds
     * @return BaseNextrasEntity|URL|null
     */
    public function getBy(array $conds)
    {
        return parent::getBy($conds);
    }

    /**
     * @param $id
     * @return BaseNextrasEntity|false|URL|null
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

}