<?php
class BaseNextrasMapper extends \Nextras\Orm\Mapper\Mapper{

    public function getConnection(){
        return $this->connection;
    }

    public function getSqlBuilder(){
        return $this->builder();
    }
}