<?php
namespace BiteIT;

use Nette\Security as NS;
use Nette\Security\IAuthenticator;
use Nette\Utils\DateTime;
use Nextras\Orm\Collection\ICollection;

/**
 * Class OrderRepository
 * @package BiteIT
 * @method ICollection|Order[] getResortOrders($resortId)
 */
class UserRepository extends BaseNextrasRepository implements IAuthenticator {

    /**
     * Returns possible entity class names for current repository.
     * @return string[]
     */
    public static function getEntityClassNames(): array
    {
        return [User::class];
    }

    function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;

        $user = $this->getBy(['email' => $email]);

        if (!$user || !NS\Passwords::verify($password, $user->password)) {
            throw new NS\AuthenticationException('Neplatný e-mail nebo heslo.');
        }

        if (!$user->active) {
            throw new NS\AuthenticationException('Your account has not been activated yet. Please confirm your account with the link provided in the e-mail.');
        }

        $user->dateLogged = new DateTime();
        $this->persistAndFlush($user);

        return new NS\Identity($user->id);
    }

    public function createUser($email, $password, $extraData = []){
        $pass = NS\Passwords::hash($password);
        $row = $this->getBy(['email' => $email]);
        if(!$row)
        {
            $user = new User();
            $user->load($extraData);
            $user->email = $email;
            $user->password = $pass;

            $this->persistAndFlush($user);
            return $user->getPersistedId();
        }
        else{
            throw new NS\AuthenticationException('Uživatel s tímto e-mailem již existuje.');
        }
    }

}