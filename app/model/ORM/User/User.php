<?php
namespace BiteIT;


/**
 * @property-read   int     $id     {primary}
 * @property string     $email
 * @property string     $password
 * @property string     $firstname
 * @property string     $surname
 * @property \DateTimeImmutable  $dateLogged
 * @property bool       $active         {default true}
 * @property bool       $superadmin     {default false}
 */
class User extends BaseNextrasEntity
{
    public function getFullName(){
        return $this->firstname.' '.$this->surname;
    }

    /**
     * @return Resort|null
     */
    public function getAssignedResort(){
        if($this->superadmin)
            return false;

        return $this->getRepository()->getModel()->resorts->getBy(['admin' => $this->id]);
    }

}
