<?php
/**
 * User: Manwe
 */

namespace Cerebro;

use Nette\Forms\Controls\Checkbox;
use Nette\Utils\Html;

class HappyCheckbox extends Checkbox
{
    protected $wrapper;
    protected $color;


    public function __construct($color = 'success')
    {
        parent::__construct();
        $this->wrapper = Html::el();
        $this->color = $color;
    }

    public function getControl()
    {
        return $this->wrapper->setHtml($this->getLabelPart()->insert(0, $this->getControlPart()->addClass('happy '.$this->color)));
    }

}