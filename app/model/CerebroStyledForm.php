<?php

class CerebroStyledForm extends ExtForm
{
    public function __construct(Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $renderer = $this->getRenderer();
        $renderer->wrappers['controls']['container'] = null;
        $renderer->wrappers['form']['container'] = 'div class=col-md-6';

        $renderer->wrappers['label']['container'] = null;
        $renderer->wrappers['control']['container'] = null;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';

        $this->elementPrototype->setAttribute('class', 'row');
    }

    public function setFullWidth(){
        $renderer = $this->getRenderer();
        $renderer->wrappers['form']['container'] = 'div class=col-md-12';
        $this->setNumberOfColumns(2);
    }

    public function setNumberOfColumns($number){
        $renderer = $this->getRenderer();
        $renderer->wrappers['pair']['container'] = 'div class="form-group col-md-'.(12/$number).'"';
    }
}