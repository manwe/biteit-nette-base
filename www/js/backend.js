/**!*/


/**
 * Funkce s moznosti registrace this properties
 */
(function ($, undefined) {

    $.nette.ext('spinner', {
        init: function (x) {
            this.spinner = this.createSpinner();
            this.spinner.appendTo('body');
        },
        start: function (jqXHR, settings) {
            this.spinner.show(this.speed);
        },
        complete: function () {
            this.spinner.hide(this.speed);
            $(".modal").modal();
        }
    }, {
        createSpinner: function () {
            var spinner = $('<div id="ajax-cover"><div id="ajax-spinner"><span class="fa fa-spinner fa-spin fa-2x"></span></div></div>');
            return spinner;
        },
        spinner: null,
        speed: 0
    });

    $.nette.ext('watafak', {
        complete: function(){
            initStuff();
        }
    });

    $(document).on('click', function(e){
        if( $('.modal').length > 0 && !$('.modal-content').includePoint(e.pageX, e.pageY) ){
            setTimeout(function(){
                $('.modal-content').remove();
            }, 400);
        }
    });

    $(window).on('scroll', function(){
        checkStickyDatagridHeaders();
    });


})(jQuery);

$(document).ready(function(){
    initStuff();
})

var lastClickedSubmit, body;

function initStuff() {
    initGalleries();
}

function initGalleries(){
    $('.images').magnificPopup({
        type: 'image',
        delegate: 'a.magnific',
        gallery: {
            enabled: true
        }
    });
}
function checkStickyDatagridHeaders(){
    $('.datagrid:not(.has-sticky-header)').each(function(){
        var sourceDatagrid = $(this);
        var uid = generateUUID();
        $(this).data('sticky-id', uid);
        $(this).addClass('has-sticky-header');
        var header = $(this).find('table thead').clone();
        var table = $('<table class="datagrid-header" id="'+uid+'"></table>');
        table.append(header);
        $(table).css({position: 'fixed', top: '0px', left: $(this).offset().left+14,
                'backgroundColor': $('body').css('background-color'), display: 'none' });

        var n = 0;
        $(table).find('th').each(function(){
            n++;
            var mirrorEl = $(sourceDatagrid).find('thead th:nth-child('+n+')');
            $(this).css({'width': mirrorEl.outerWidth(), 'fontSize': mirrorEl.css('font-size'),
                        'padding': mirrorEl.css('padding')
            });
            // console.log(n+'th TH has width of '+$(sourceDatagrid).find('thead th:nth-child('+n+')').outerWidth());
        });

        $('body').append(table);

    });

    $('.datagrid.has-sticky-header').each(function(){

        if( $(window).scrollTop() > $(this).find('table thead').offset().top ) {
            $('.datagrid-header#' + $(this).data('sticky-id')).show();
        }
        else {
            $('.datagrid-header#' + $(this).data('sticky-id')).hide();
        }

    });


}



$(function(){
    tinymce.init({
        selector: 'textarea.tinymce',
        language: 'cs',
        skin: 'charcoal'
    });
})