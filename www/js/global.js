if(! jQuery.isFunction(jQuery.fn.includePoint) )
{
    jQuery.fn.includePoint = function (x,y)
    {
        if(x >= $(this).offset().left && x <= $(this).offset().left + $(this).outerWidth() &&
            y >= $(this).offset().top && y <= $(this).offset().top + $(this).outerHeight() )
            return true;
        return false;
    };
}


$(function(){

    $.nette.init();

    $.nette.ext('myprogress', {
        before : function(xhr, settings){
            var myext = this;
            settings.xhr = function() {
                var xhrobj = jQuery.ajaxSettings.xhr();
                if (xhrobj.upload) {
                    xhrobj.upload.addEventListener("progress", function(event)
                    {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }

                        myext.onprogress(percent);

                    }, false);
                }

                return xhrobj;
            };
        }

    },{
        onprogress: function(percent){
            if(percent === 100)
                console.log('State of upload: '+percent);
        },
        createProgressBar: function(){

        },
        hideProgressBar: function(){

        }
    });
    //
    // $.nette.ext('triggerFunctions', {
    //     before: function(jqXHR, settings){
    //     },
    //     start: function(a, b, c){
    //     },
    //     complete: function () {
    //         disableLabelsForDisabledSubmits();
    //     }
    // });
    //
    // disableLabelsForDisabledSubmits();



    $("[name^='date'], .datepicker").datepicker();
});

function disableLabelsForDisabledSubmits(){
    var label;
    var submits = $("[type='submit'][id]");
    submits.each(function(){
        label = $("[for='"+$(this).attr("id")+"']");
        if(label.length)
        {
            if($(this).is(":disabled"))
                label.addClass("disabled");
            else
                label.removeClass("disabled");
        }
    });
}



Nette.showFormErrors = function(form, errors) {
    var messages = [],
        focusElem;

    $(form).find("*").removeClass("hasError");
    for (var i = 0; i < errors.length; i++) {
        var elem = errors[i].element,
            message = errors[i].message;

        if (!Nette.inArray(messages, message)) {
            messages.push(message);

            if (!focusElem && elem.focus) {
                focusElem = elem;
            }
        }

        $(elem).addClass("hasError");
    }

    if (messages.length) {
        showFlashMessage(messages.join('<br>'), 'error');

        if (focusElem) {
            focusElem.focus();
        }
    }
};

function showFlashMessage(message, type){
    var icon = 'info';
    if(type == 'error')
        icon = 'warning';
    else if(type == 'ok')
        icon = 'check';

    var html = '<div class="flash '+type+'">' +
            '<span class="flash-close fa-lg fa fa-close"></span>' +
            '<table><tr><td class="flash-icon"><span class="fa fa-2x fa-'+icon+'"></span></td><td class="flash-text">'+
            '<p>'+message+'</p></td></tr><tr><td colspan="2"><div class="timer-background"><div class="timer"></div></div>'+
            '</td></tr></table></div>';

    $('#flash-wrapper').append(html);

    initFlashes();
}

function initFlashes() {
    $('.flash').each(function () {
        var e = $(this);
        var len = $(this).find('.flash-text').text().length;
        var animationLength = 3500+len*60;
        // setTimeout(function () {
        //
        // }, animationLength);

        $(this).click(function () {
            $(this).fadeOut(200, function () {
                $(this).remove();
            });
        });

        $(this).on('mouseenter', function () {
            $(this).find('.timer').stop();
        });

        $(this).find('.timer').animate({width: 0}, animationLength, 'linear', function(){
            $(e).fadeOut(200, function () {
                $(this).remove();
            });
        });
    });
}
//
//
// $.datepicker.regional.cs = {
//     closeText: 'Cerrar',
//     prevText: 'Předchozí',
//     nextText: 'Další',
//     currentText: 'Hoy',
//     monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
//     monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'],
//     dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'],
//     dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',],
//     dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'],
//     weekHeader: 'Sm',
//     dateFormat: 'dd.mm.yy',
//     firstDay: 1,
//     isRTL: false,
//     showMonthAfterYear: false,
//     yearSuffix: ''};
//
// $.datepicker.setDefaults($.datepicker.regional['cs']);
